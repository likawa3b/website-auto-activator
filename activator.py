import schedule
import time
import datetime
# import subprocess
# import urllib.request
from selenium import webdriver
import requests
import smtp_gmail as email




hostname = [
    'https://linksone-admin-qa1.azurewebsites.net/auth/login',
    'https://linksone-admin-staging1.azurewebsites.net/auth/login',
    'https://qa1-admin-api.azurewebsites.net/',
    'https://qa1-client-api.azurewebsites.net/',
    # 'https://stage-client4.azurewebsites.net/auth/login',
    'https://staging1-admin-api.azurewebsites.net/',
    'https://staging1-client-api.azurewebsites.net/',
    'https://uat-client4.azurewebsites.net/auth/login',
    'https://qa-leave.azurewebsites.net/leave/admin/',
    'https://qa-workflow.azurewebsites.net',
]

period = schedule.every(10).minutes
# schedule.every(10).minutes
# schedule.every(10).seconds
# schedule.every().hour

global browser
browser = webdriver.Chrome("./chromedriver")

headers = {'Accept-Encoding': 'identity'}
def job():
    alert = False
    print('Requesting websites...')
    out = open('output.txt', 'a')
    out.write(
        '=======================================================================\n'
        + str(datetime.datetime.now()) + '\nRequesting websites...\n')
    for address in hostname:
        # subprocess.call(['ping', address], stdout=out)
        # page = urllib.request.urlopen(address)
        try:
            print('Requesting ' + address)
            browser.get(address)
            content_type = requests.head(address,headers=headers)
            print(content_type)
            out.write('Requesting ' + address + ' : '+str(content_type)+'\n')
        except Exception as error:
            out.write('Requesting ' + address + ' : ' + str(error) + '\n')
            if (not alert):
                title = 'Web Activator Alert'
                content = 'Web Activator Error!'
                out.write('Send Email Success') if email.sendAlertToHarry(title, content) else out.write('Send Email Fail')
                alert = True

    out.write('Done!\n')
    out.close()
    print('Done!')


job()

period.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)